
#include <Arduino.h>
#include <avr/wdt.h>
#include "DRV8834.h"
#include "SparkFunBME280.h"
#include <Wire.h>
#include <SPI.h>
#define DIR 2
#define STEP 3
#define M0 8
#define M1 7

#define SLEEP 4 // optional (just delete SLEEP from everywhere if not used)
#define pinIRd 12
#define pinIRa A0

//Global sensor object
BME280 mySensorA;


#define RPM 10
#define MOTOR_STEPS 200


int IRvalueA;
int IRvalueD;
String data ;
String label1 = "*IDN?";
String label2 = "SER?";
String label3 = "WTF?";
String label4 = "MOVE";
String label5 = "SPEED";
String label6 = "HOME";
String label7 = "OPC?";
String label8 = "SENSOR?";
String label9 = "CALIBRATION";
String label10 = "STATE";
String label11 = "RESET";
String label12 = "TEMPERATURE?";
String label13 = "HUMIDITY?";
String label14 = "ALTITUDE?";
String label15 = "PRESSURE?";
String label16 = "MEAS?";
String idn = "Williot Turntable ver 0.4";
int serial = 1005;




DRV8834 stepper(MOTOR_STEPS, DIR, STEP, SLEEP, M0, M1);

void(* resetFunc) (void) = 0;//declare reset function at address 0

void softwareReset( uint8_t prescaller) {
  // start watchdog with the provided prescaller
  wdt_enable( prescaller);
  // wait for the prescaller time to expire
  // without sending the reset signal by using
  // the wdt_reset() method
  while(1) {}
}

void setup() {



    //***Set up sensor 'A'******************************//
    //commInterface can be I2C_MODE or SPI_MODE
    mySensorA.settings.commInterface = I2C_MODE;
    mySensorA.settings.I2CAddress = 0x77;
    mySensorA.settings.runMode = 3; //  3, Normal mode
    mySensorA.settings.tStandby = 0; //  0, 0.5ms
    mySensorA.settings.filter = 0; //  0, filter off
    //tempOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
    mySensorA.settings.tempOverSample = 1;
    //pressOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
      mySensorA.settings.pressOverSample = 1;
    //humidOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
    mySensorA.settings.humidOverSample = 1;
  
    Serial1.setTimeout(10);
    
    stepper.begin(RPM);
    stepper.enable();
    stepper.setMicrostep(32);   // Set microstep mode to 1:32 
    pinMode(pinIRd,INPUT);
    pinMode(pinIRa,INPUT);
    Serial1.begin(2400);  // Start the Serial1 monitor with speed of 9600 Bauds
    delay(1);
    Serial1.print("Sensor A: 0x");
    Serial1.println(mySensorA.begin(), HEX);
    Serial1.println(idn);
    delay(5); 
}

void en_dis(bool state) {
  if (state==0){
    stepper.disable();
    Serial1.println("Stepper disable");
    }
  
  else if (state==1){
    stepper.enable();
    Serial1.println("Stepper enabled");
    }
 }

void Calibration() {
  while (digitalRead(pinIRd)>0){
    stepper.move(10);
    } 
   Serial1.println("Finished move");

}

void MoveToPosition(int pos){
    stepper.move(pos);
    
    Serial1.println("Finished move");
  }
void Homing(){
    Serial1.print("GOING HOME - NEED TO ADD LOGIC ");
    Serial1.print('\n');
  }

void loop() {
 while (Serial1.available()>0)  { // Check if values are available in the Serial1 Buffer
      data = Serial1.readString();        //Read the incoming data & store into data
      if (data==label1){
        Serial1.println(idn);
        }
      else if (data==label2){
        Serial1.println(serial);
        }
      else if (data==label9){
        Calibration();
        }
      else if (data==label8){
        IRvalueA = analogRead(pinIRa);
        IRvalueD = digitalRead(pinIRd);
        Serial1.print(IRvalueA);
        Serial1.print("\t");
        Serial1.println(IRvalueD);
        }
      else if (data==label3){
         Serial1.println(label3);
        }
      else if (data==label6){
         Homing();
        }
      else if (data==label7){
         Serial1.println(stepper.getStepsRemaining());
         Serial1.println(label7);
        }
      else if (data.substring(0,4)==label4){
         MoveToPosition(data.substring(5).toInt());
         // Set the speed in steps per second:
         delay(5);
        }
      else if (data.substring(0,5)==label10){
         en_dis(data.substring(6).toInt());
         
         
         // Set the speed in steps per second:
         delay(5);
        }
      else if (data==label11){
         softwareReset(WDTO_60MS); //call reset 
        }
      else if (data==label12){
        Serial1.print("Temperature: ");
        Serial1.print(mySensorA.readTempC(), 2);
        Serial1.println(" degrees C");
        Serial1.print("Temperature: ");
        Serial1.print(mySensorA.readTempF(), 2);
        Serial1.println(" degrees F");
        }

      else if (data==label13){
        Serial1.print("%RH: ");
        Serial1.print(mySensorA.readFloatHumidity(), 2);
        Serial1.println(" %");
        }
      else if (data==label14){
        Serial1.print("Altitude: ");
        Serial1.print(mySensorA.readFloatAltitudeMeters(), 2);
        Serial1.println("m");
        Serial1.print("Altitude: ");
        Serial1.print(mySensorA.readFloatAltitudeFeet(), 2);
        Serial1.println("ft"); 

        }
      else if (data==label15){
        Serial1.print("Pressure: ");
        Serial1.print(mySensorA.readFloatPressure(), 2);
        Serial1.println(" Pa");
      }
      else if (data==label16){
        Serial1.print(mySensorA.readTempC(), 2);
        Serial1.print(", ");
        Serial1.print(mySensorA.readTempF(), 2);
        Serial1.print(", ");        
        Serial1.print(mySensorA.readFloatHumidity(), 2);
        Serial1.print(", ");
        Serial1.print(mySensorA.readFloatAltitudeMeters(), 2);
        Serial1.print(", ");
        Serial1.print(mySensorA.readFloatAltitudeFeet(), 2);
        Serial1.print(", ");
        Serial1.println(mySensorA.readFloatPressure(), 2);

      }





        
      else{
         Serial1.println(data);
         Serial1.println("Not IDENTIEFIED");
      }
  }
}
