
#include <Arduino.h>
#include <avr/wdt.h>

String data;
String label1 = "*IDN?";
String label2 = "SER?";
String label3 = "GPIO_DIRECT";
String label4 = "GPIO_BUFFERED";
String label5 = "SMU_OUT";
String label6 = "GPIO_IN";


String label11 = "RESET";
String idn = "Williot GPIO_Router ver 0.2";
int serial = 1001;
unsigned char relayPin[4] = {4,7,8,12};

void(* resetFunc) (void) = 0;//declare reset function at address 0

void softwareReset( uint8_t prescaller) {
  // start watchdog with the provided prescaller
  wdt_enable( prescaller);
  // wait for the prescaller time to expire
  // without sending the reset signal by using
  // the wdt_reset() method
  while(1) {}
}
void Default()
{
  digitalWrite(relayPin[0],LOW);
  digitalWrite(relayPin[2],LOW);
  }

void setup() {
    Serial.setTimeout(10);
    Serial.begin(1000000);  // Start the Serial monitor with speed of 9600 Bauds
    delay(1);
    Serial.println(idn);
    delay(5); 
    int i;
    for(i = 0; i < 4; i++)
    {
      pinMode(relayPin[i],OUTPUT);
    }
    Default();
    }

void loop() {
 while (Serial.available()>0)  { // Check if values are available in the Serial Buffer
      data = Serial.readString();        //Read the incoming data & store into data
      if (data==label1){
        Serial.println(idn);
        }
      else if (data==label2){
        Serial.println(serial);
        }
      else if (data==label3){

         digitalWrite(relayPin[0],LOW);
         digitalWrite(relayPin[1],LOW);
         digitalWrite(relayPin[2],LOW);
         digitalWrite(relayPin[3],LOW);
        }
      else if (data==label4){

         digitalWrite(relayPin[0],LOW);
         digitalWrite(relayPin[1],LOW);
         digitalWrite(relayPin[2],HIGH);
         digitalWrite(relayPin[3],HIGH);
              }
      else if (data==label5){
         digitalWrite(relayPin[0],HIGH);
         digitalWrite(relayPin[1],LOW);
         digitalWrite(relayPin[2],LOW);
         digitalWrite(relayPin[3],LOW);
              }
     else if (data==label6){
         digitalWrite(relayPin[0],HIGH);
         digitalWrite(relayPin[1],HIGH);
         digitalWrite(relayPin[2],LOW);
         digitalWrite(relayPin[3],LOW);
              }

      else if (data==label11){
         softwareReset(WDTO_60MS); //call reset 
        }

      else
       {

      }
  }
}
