
#include <Arduino.h>
// Motor steps per revolution. Most steppers are 200 steps or 1.8 degrees/step
#define MOTOR_STEPS 200
#define RPM 10
#define DIR 2
#define STEP 3
#define SLEEP 4 // optional (just delete SLEEP from everywhere if not used)
#define pinIRd 12
#define pinIRa A0
/*
 * Choose one of the sections below that match your board
 */
#include "DRV8834.h"
#define M0 8
#define M1 7
DRV8834 stepper(MOTOR_STEPS, DIR, STEP, SLEEP, M0, M1);

// #include "A4988.h"
// #define MS1 10
// #define MS2 11
// #define MS3 12
// A4988 stepper(MOTOR_STEPS, DIR, STEP, SLEEP, MS1, MS2, MS3);

// #include "DRV8825.h"
// #define MODE0 10
// #define MODE1 11
// #define MODE2 12
// DRV8825 stepper(MOTOR_STEPS, DIR, STEP, SLEEP, MODE0, MODE1, MODE2);

// #include "DRV8880.h"
// #define M0 10
// #define M1 11
// #define TRQ0 6
// #define TRQ1 7
// DRV8880 stepper(MOTOR_STEPS, DIR, STEP, SLEEP, M0, M1, TRQ0, TRQ1);

// #include "BasicStepperDriver.h" // generic
// BasicStepperDriver stepper(DIR, STEP);



String data ;
String label1 = "*IDN?";
String label2 = "SER?";
String label3 = "WTF?";
String label4 = "MOVE";
String label5 = "SPEED";
String label6 = "HOME";
String label7 = "OPC?";
String label8 = "SENSOR?";
String idn = "Williot Turntable ver 0.3\n";
int serial = 1002;

void setup() {
      /*
     * Set target motor RPM.
     */
    stepper.begin(RPM);
    // if using enable/disable on ENABLE pin (active LOW) instead of SLEEP uncomment next line
    // stepper.setEnableActiveState(LOW);
    stepper.enable();
    // set current level (for DRV8880 only). 
    // Valid percent values are 25, 50, 75 or 100.
    // stepper.setCurrent(100);
    stepper.setMicrostep(32);   // Set microstep mode to 1:32
    
   pinMode(pinIRd,INPUT);
   pinMode(pinIRa,INPUT);
   Serial.begin(9600);  // Start the Serial monitor with speed of 9600 Bauds
   Serial.print(idn);
   delay(5);
   
}




void MoveToPosition(int pos){
     Serial.print("MOVE to POS ");
     Serial.print(pos);
     Serial.print('\n');
     stepper.move(pos);
     Serial.print("Finished move");
     Serial.print('\n');
  }
void Homing(){
    Serial.print("GOING HOME - NEED TO ADD LOGIC ");
    Serial.print('\n');
  }

void loop() {
 while (Serial.available()>0)  { // Check if values are available in the Serial Buffer
      data = Serial.readString();        //Read the incoming data & store into data
      Serial.print(data);          //Print Value inside data in Serial monitor
      Serial.print("\n");  
      if (data==label1){
        Serial.println(idn);
        Serial.print('\n');
        }
      else if (data==label2){
        Serial.println(serial);
        Serial.print('\n');
        }
      else if (data==label8){

        Serial.print("Analog Reading=");
        Serial.print("\t Digital Reading=");
        }
      else if (data==label3){
         Serial.println(label3);
         Serial.print('\n');
        }
      else if (data==label6){
         Homing();
        }
      else if (data==label7){
         Serial.println(stepper.getStepsRemaining());
         Serial.println(label7);
         Serial.print('\n');
        }
      else if (data.substring(0,4)==label4){
         MoveToPosition(data.substring(5).toInt());
         // Set the speed in steps per second:
         Serial.print('\n');
         delay(5);
         
        }
      else{
         Serial.println(data.substring(0,3));
         Serial.println("Not iDENTIEFIED");
      }
  }
}
