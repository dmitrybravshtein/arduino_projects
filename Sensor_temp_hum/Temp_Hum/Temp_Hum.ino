
#include <avr/wdt.h>
#include <stdint.h>
#include "SparkFunBME280.h"

#include "Wire.h"
#include "SPI.h"

//Global sensor object
BME280 mySensorA;
BME280 mySensorB;


String label1 = "*IDN?";
String label2 = "TEMPERATURE?";
String label3 = "HUMIDITY?";
String label4 = "ALTITUDE?";
String label5 = "PRESSURE?";
String label6 = "MEAS?";
String label7 = "SER?";
String label10 = "STATE";
String label11 = "RESET";
String data ;
String idn = "Williot Temperature and Humidity 0.1";
int serial = 0001;

void(* resetFunc) (void) = 0;//declare reset function at address 0

void softwareReset( uint8_t prescaller) {
  // start watchdog with the provided prescaller
   wdt_enable( prescaller);
  // wait for the prescaller time to expire
  // without sending the reset signal by using
  // the wdt_reset() method
  while(1) {}
}

void setup() {

  //***Set up sensor 'A'******************************//
    //commInterface can be I2C_MODE or SPI_MODE
    mySensorA.settings.commInterface = I2C_MODE;
    mySensorA.settings.I2CAddress = 0x77;
    mySensorA.settings.runMode = 3; //  3, Normal mode
    mySensorA.settings.tStandby = 0; //  0, 0.5ms
    mySensorA.settings.filter = 0; //  0, filter off
    //tempOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
    mySensorA.settings.tempOverSample = 1;
    //pressOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
      mySensorA.settings.pressOverSample = 1;
    //humidOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
    mySensorA.settings.humidOverSample = 1;
    //***Set up sensor 'B'******************************//
    //commInterface can be I2C_MODE or SPI_MODE
    //specify chipSelectPin using arduino pin names
    //specify I2C address.  Can be 0x77(default) or 0x76
    mySensorB.settings.commInterface = SPI_MODE;
    mySensorB.settings.chipSelectPin = 10;
    mySensorB.settings.runMode = 3; //  3, Normal mode
    mySensorB.settings.tStandby = 0; //  0, 0.5ms
    mySensorB.settings.filter = 0; //  0, filter off
    //tempOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
    mySensorB.settings.tempOverSample = 1;
    //pressOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
      mySensorB.settings.pressOverSample = 1;
    //humidOverSample can be:
    //  0, skipped
    //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
    mySensorB.settings.humidOverSample = 1;

    Serial.begin(1000000);  // Start the Serial monitor with speed of 9600 Bauds
    Serial.setTimeout(10);
//    Serial.print("Program Started\n");
//    Serial.println("Starting BME280s... result of .begin():");
    delay(10);  //Make sure sensor had enough time to turn on. BME280 requires 2ms to start up.
    //Calling .begin() causes the settings to be loaded 
    Serial.print("Sensor A: 0x");
    Serial.println(mySensorA.begin(), HEX);
    Serial.print("Sensor B: 0x");
    Serial.println(mySensorB.begin(), HEX);
    Serial.println(idn);



}

void loop() {
 while (Serial.available()>0)  { // Check if values are available in the Serial Buffer
      data = Serial.readString();        //Read the incoming data & store into data
      if (data==label1){
        Serial.println(idn);
        }
      else if (data==label7){
        Serial.println(serial);
        }
      else if (data==label2){
        Serial.print("Temperature: ");
        Serial.print(mySensorA.readTempC(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readTempC(), 2);
        Serial.println(" degrees C");
        Serial.print("Temperature: ");
        Serial.print(mySensorA.readTempF(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readTempF(), 2);
        Serial.println(" degrees F");
        }

      else if (data==label3){
        Serial.print("%RH: ");
        Serial.print(mySensorA.readFloatHumidity(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readFloatHumidity(), 2);
        Serial.println(" %");
        }
      else if (data==label4){
        Serial.print("Altitude: ");
        Serial.print(mySensorA.readFloatAltitudeMeters(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readFloatAltitudeMeters(), 2);
        Serial.println("m");
        Serial.print("Altitude: ");
        Serial.print(mySensorA.readFloatAltitudeFeet(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readFloatAltitudeFeet(), 2);
        Serial.println("ft"); 

        }
      else if (data==label5){
        Serial.print("Pressure: ");
        Serial.print(mySensorA.readFloatPressure(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readFloatPressure(), 2);
        Serial.println(" Pa");
      }
      else if (data==label6){
        Serial.print(mySensorA.readTempC(), 2);
        Serial.print(", ");
        Serial.print(mySensorA.readTempF(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readTempC(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readTempF(), 2);
        Serial.print(", ");
        Serial.print(mySensorA.readFloatHumidity(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readFloatHumidity(), 2);
        Serial.print(", ");
        Serial.print(mySensorA.readFloatAltitudeMeters(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readFloatAltitudeMeters(), 2);
        Serial.print(", ");
        Serial.print(mySensorA.readFloatAltitudeFeet(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readFloatAltitudeFeet(), 2);
        Serial.print(", ");
        Serial.print(mySensorA.readFloatPressure(), 2);
        Serial.print(", ");
        Serial.print(mySensorB.readFloatPressure(), 2);
        Serial.println("");
      }
      else if (data==label11){
         softwareReset(WDTO_60MS); //call reset 
        }     
      else {
         Serial.println(data);
         Serial.println("Not IDENTIEFIED");
      }
  }
}
