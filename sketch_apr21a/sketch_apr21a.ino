#include <avr/wdt.h>
#define gpio1 2
#define gpio2 3
#define gpio3 4
#define gpio4 5
#define gpio5 6
#define gpioLED 13
String label1 = "*IDN?";
String label2 = "SER?";
String label3 = "GPIO1_ON";
String label4 = "GPIO1_OFF";
String label5 = "GPIO1_PULSE";
String label6 = "GPIO2_ON";
String label7 = "GPIO2_OFF";
String label8 = "GPIO2_PULSE";
String label9 = "GPIO3_ON";
String label12 = "GPIO3_OFF";
String label13 = "GPIO3_PULSE";
String label14 = "GPIO4_ON";
String label15 = "GPIO4_OFF";
String label16 = "GPIO4_PULSE";
int State = 0;
String label10 = "STATE";
String label11 = "RESET";
String data ;
String idn = "Williot Tester_Demo";
int serial = 1;
bool ran;
void(* resetFunc) (void) = 0;//declare reset function at address 0

void softwareReset( uint8_t prescaller) {
  // start watchdog with the provided prescaller
   wdt_enable( prescaller);
  // wait for the prescaller time to expire
  // without sending the reset signal by using
  // the wdt_reset() method
  while(1) {}
}

void setup() {
    Serial.begin(9600);  // Start the Serial monitor with speed of 9600 Bauds
    Serial.setTimeout(10);
    Serial.println(idn);
    
//    delay(5); 
    pinMode(gpio1,OUTPUT);
    pinMode(gpio2,OUTPUT);
    pinMode(gpio5,OUTPUT);
    pinMode(gpioLED,OUTPUT);
    digitalWrite(gpio1,HIGH);
    digitalWrite(gpio2,HIGH);
    digitalWrite(gpio5,HIGH);
    digitalWrite(gpioLED,LOW);
    delay(5); 
    pinMode(gpio3, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(gpio3), new_batch, RISING);
    pinMode(gpio4, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(gpio4), start, RISING);
    Serial.flush();
}

void pulseOut(int pin, int us)
{
   digitalWrite(pin, LOW);
   digitalWrite(13, LOW);
  // us = max(us - 20, 1);
   delay(us);
   digitalWrite(pin, HIGH);
   digitalWrite(13, HIGH);
}  

void new_batch(){
   detachInterrupt(digitalPinToInterrupt(gpio3));
   Serial.println(label2);
   State = 1; 
  }
void start(){
  detachInterrupt(digitalPinToInterrupt(gpio4));
  Serial.println(label2);
  State = 2; 
  }

void loop() {
  if (State !=0){
    switch(State){
      case 1:
          Serial.println(State);
          Serial.println(2000);
          delay(2000);
          pulseOut(2,50);
          State = 0;
          attachInterrupt(digitalPinToInterrupt(gpio3), new_batch, RISING);
         break;
      case 2:
          Serial.println(State);
          Serial.println(100);
          delay(100);

          if (ran==true){
             pulseOut(3,50);
             Serial.println(3);
             ran =false;
             attachInterrupt(digitalPinToInterrupt(gpio4), start, RISING);
            }
           else{
             pulseOut(5,50);
             Serial.println(5);
             ran = true;
             attachInterrupt(digitalPinToInterrupt(gpio4), start, RISING);
            }
          State = 0;
         break;
      }
    }
  else if (Serial.available()>0)  { // Check if values are available in the Serial Buffer      
      data = Serial.readString();  
      if (data==label1){
        Serial.println(idn);
        }
      else if (data==label2){
        Serial.println(serial);
        }
      else if (data==label3){
        digitalWrite(gpio1,HIGH);
        digitalWrite(13,HIGH);
        Serial.println("GPIO State PASS");
        }
      else if (data==label4){
        digitalWrite(gpio1,LOW);
        digitalWrite(13,LOW);
        Serial.println("GPIO State Fail");
        }
      else if (data==label6){
        digitalWrite(gpio2,HIGH);
        digitalWrite(13,HIGH);
        Serial.println("GPIO State PASS");
        }
      else if (data==label7){
        digitalWrite(gpio2,LOW);
        digitalWrite(13,LOW);
        Serial.println("GPIO State Fail");
        }
      else if (data==label9){
        digitalWrite(gpio3,HIGH);
        digitalWrite(13,HIGH);
        Serial.println("GPIO State PASS");
        }
      else if (data==label12){
        digitalWrite(gpio3,LOW);
        digitalWrite(13,LOW);
        Serial.println("GPIO State Fail");
        }
      else if (data==label14){
        digitalWrite(gpio4,HIGH);
        digitalWrite(13,HIGH);
        Serial.println("GPIO State PASS");
        }
      else if (data==label15){
        digitalWrite(gpio4,LOW);
        digitalWrite(13,LOW);
        Serial.println("GPIO State Fail");
        }

        
      else if (data==label11){
         softwareReset(WDTO_60MS); //call reset 
        }
      else if (data.substring(0,11)==label5){ 
         pulseOut(gpio1,data.substring(12).toInt()); 
         Serial.println("Completed Successfully");   
        }
      else if (data.substring(0,11)==label8){ 
         pulseOut(gpio2,data.substring(12).toInt()); 
         Serial.println("Completed Successfully");   
        }
      else if (data.substring(0,11)==label13){ 
         pulseOut(gpio3,data.substring(12).toInt()); 
         Serial.println("Completed Successfully");   
        }
      else if (data.substring(0,11)==label16){ 
         pulseOut(gpio4,data.substring(12).toInt()); 
         Serial.println("Completed Successfully");   
        }

        
      else {
         Serial.println(data);
         Serial.println("Not IDENTIEFIED");
      }
  }
   
}
